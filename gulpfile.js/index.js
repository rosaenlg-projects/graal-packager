const { src, dest, parallel, series } = require('gulp');
const fs = require('fs');
const { exec } = require('child_process');
const gulpRosaenlgHelpers = require('gulp-rosaenlg');

const rosaeNLGVersion = "1.0.5";

function init(cb) {
  const folders = [
    'dist/',
  ];

  folders.forEach(dir => {
      if(!fs.existsSync(dir)) {
          fs.mkdirSync(dir);
          console.log('📁  folder created:', dir);    
      }   
  });
  cb();
}

function clean(cb) {
  exec('rm -rf dist');
  cb();
}

function copyJsLibs() {
  return src([
    `node_modules/rosaenlg/dist/browser/rosaenlg_tiny_en_US_${rosaeNLGVersion}.js`
    ])
    .pipe(dest('dist'));
}

function templates() {
  return gulpRosaenlgHelpers.compileTemplates(
    [ { source: 'src/templates/tutorial_en_US.pug', name: 'tutorial_en_US' } ],
    'en_US',
    'dist/tutorial_en_US.js',
    'templates_holder',
    true
  )
}

exports.init = init;
exports.clean = clean;


exports.all = series(clean, init, copyJsLibs, templates);

