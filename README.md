# Graal Packager

Packages the RosaeNLG tutorial in a js file.
Designed to prepare the templates so that they can run in GraalVM.

Use along with https://gitlab.com/rosaenlg-projects/rosaenlg-graal-poc